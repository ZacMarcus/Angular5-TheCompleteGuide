import { EventEmitter } from '@angular/core';

export class LoggingService {
  activeToInactiveUpdated = new EventEmitter<number>();
  inactiveToActiveUpdated = new EventEmitter<number>();

  activeToInactiveCount = 0;
  inactiveToActiveCount = 0;

  activeToInactive() {
    this.activeToInactiveCount += 1;
    this.activeToInactiveUpdated.emit(this.activeToInactiveCount);
  }

  inactiveToActive() {
    this.inactiveToActiveCount += 1;
    this.inactiveToActiveUpdated.emit(this.inactiveToActiveCount);
  }
}
