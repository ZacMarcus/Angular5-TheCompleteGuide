import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { LoggingService } from './logging.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  activeToInactive = 0;
  inactiveToActive = 0;

  constructor(private loggingService: LoggingService) {

  }

  ngOnInit() {
    this.loggingService.activeToInactiveUpdated.subscribe(
      (value: number) => this.activeToInactive = value
    );

    this.loggingService.inactiveToActiveUpdated.subscribe(
      (value: number) => this.inactiveToActive = value
    );
  }
}
