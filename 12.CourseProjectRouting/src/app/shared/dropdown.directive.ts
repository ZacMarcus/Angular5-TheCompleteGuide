import { Directive, HostListener, HostBinding } from '@angular/core';


@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  @HostBinding('class.open') isOpen = false;
  // @HostBinding('class.open') open: string;

  constructor() { }

  @HostListener('click') toggleOpen(eventData: Event) {
    this.isOpen = !this.isOpen;

    // if (this.isOpen) {
    //   this.open = '';
    // } else {
    //   this.open = 'open';
    // }
  }
}
