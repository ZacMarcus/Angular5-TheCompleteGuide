import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showPassword = true;
  logs = [];

  onDisplayDetails() {
    this.showPassword = !this.showPassword;
    this.logs.push(new Date());
  }
}
