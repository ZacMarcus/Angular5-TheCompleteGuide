import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  number = 0;
  intervalId;
  @Output() numberCreated = new EventEmitter<{number: number}>();

  constructor() { }

  ngOnInit() {
  }

  onStartGame() {
    this.intervalId = setInterval(() => { this.incrementNumber(); }, 1000);
  }

  onStopGame() {
    clearInterval(this.intervalId);
  }

  incrementNumber() {
    this.number += 1;
    this.numberCreated.emit({number: this.number});
  }
}
